class App extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            data: null,
            search: ""
        };
        fetch(
            "http://rest.ensembl.org/info/species?content-type=application/json"
        )
            .then(response => response.json())
            .then(data => this.setState({ data }));

        this.changeSearch = this.changeSearch.bind(this);
    }
    changeSearch(text) {
        this.setState({ search: text });
    }
    render() {
        if (!this.state.data) return <div />;

        var cards = this.state.data.species.map((card, index) => (
            <Card
                key={index}
                displayName={card.display_name}
                name={card.name}
                taxonId={card.taxon_id}
                strain={card.strain}
                strainCollection={card.strain_collection}
                release={card.release}
                accession={card.accession}
                groups={card.groups}
            />
        ));
        var search = this.state.search;
        cards = cards.filter(card =>
            card.props.displayName.toLowerCase().includes(search.toLowerCase())
        );
        return (
            <div className="container">
                <SearchBar changeSearch={this.changeSearch} />
                {cards}
            </div>
        );
    }
}

class SearchBar extends React.Component {
    render() {
        return (
            <input
                className="search_bar"
                type="text"
                onChange={event => this.props.changeSearch(event.target.value)}
            />
        );
    }
}

class Card extends React.Component {
    render() {
        var groups = this.props.groups.map((group, index) => (
            <div className="group" key={index}>
                {group}
            </div>
        ));
        return (
            <div className="card">
                <div className="card_top">
                    <div className="card_top_left">
                        <div className="name">{this.props.displayName}</div>
                        <div>
                            <i>{this.props.name}</i>
                        </div>
                    </div>
                    <div className="card_top_right">
                        <div>{this.props.taxonId || "taxon_id"}</div>
                    </div>
                </div>
                <div className="card_bottom">
                    <div>
                        <b>Strain:</b>&nbsp;
                        {this.props.strain ||
                            this.props.strainCollection ||
                            "strain"}
                    </div>
                    <div>
                        <b>Release:</b>&nbsp;{this.props.release || "release"}
                    </div>
                    <div>
                        <b>Accession:</b>&nbsp;
                        {this.props.accession || "accession"}
                    </div>
                    <b>Groups:</b> <br />
                    {groups}
                </div>
            </div>
        );
    }
}

ReactDOM.render(<App />, document.getElementById("root"));
